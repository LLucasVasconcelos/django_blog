from django.db import models

# Create your models here.
class ArticleModel(models.Model):
    title = models.CharField(max_length=255)
    category = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    content = models.TextField()
    created_at = models.DateTimeField()

    def __str__(self):
        return self.title