from django.test import TestCase

from blog.models import ArticleModel
from datetime import datetime
from django.utils import timezone


# Create your tests here.
class ArticleModelTests(TestCase):
    def test_article_created_sucess(self):
        ArticleModel.objects.create(title="Test", category="Test", author="Test", content="Test",created_at=datetime.now(tz=timezone.utc))
        article = ArticleModel.objects.get(title="Test")
        self.assertEqual(article.title, "Test")
        self.assertEqual(article.category, "Test")
        self.assertEqual(article.author, "Test")
        self.assertEqual(article.content, "Test")

class BlogPagesTests(TestCase):
    def test_home_page_status_code(self):
        response = self.client.get('/blog/articles')
        self.assertEqual(response.status_code, 200)